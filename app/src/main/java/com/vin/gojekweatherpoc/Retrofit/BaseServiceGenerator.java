package com.vin.gojekweatherpoc.Retrofit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vin.gojekweatherpoc.helper.AppConstants;
import com.vin.gojekweatherpoc.weatherinfo.activity.BaseActivity;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vinod on 7/7/19.
 */

public class BaseServiceGenerator {

    public static final String S_CACHE_CONTROL = "Cache-Control";
    private static String CACHE_NAME = "gojek-cache";
    private static boolean isCachingEnabled = false;


    public static Retrofit createService(Context context, final String baseUrl) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson));

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (isCachingEnabled) {
            httpClient.cache(getCache(context));
        }
        httpClient.addNetworkInterceptor(new NetworkInterceptor());
        httpClient.addInterceptor(new RequestInterceptor());
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = null;

                request = chain.request().newBuilder()
                        .method(chain.request().method(), chain.request().body())
                        .build();
                return chain.proceed(request);

            }
        });
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit;
    }

    private static Cache getCache(Context context) {
        long SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(new File(context.getCacheDir(), CACHE_NAME), SIZE_OF_CACHE);
        return cache;
    }


    public static class NetworkInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());
            // re-write response header to force use of cache
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge(10000, TimeUnit.SECONDS).build();

            return response.newBuilder()
                    .header(S_CACHE_CONTROL, cacheControl.toString()).build();
        }
    }

    public static class RequestInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (!BaseActivity.isConnectingToInternet()) {
                CacheControl cacheControl = new CacheControl.Builder().maxStale(AppConstants.INT_7,
                        TimeUnit.DAYS).build();
                request = request.newBuilder().cacheControl(cacheControl).build();
            }
            return chain.proceed(request);
        }
    }
}
