package com.vin.gojekweatherpoc.Retrofit;

import com.vin.gojekweatherpoc.weatherinfo.model.WeatherResponseModel;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by vinod on 7/7/19.
 */

public interface AuthApi {

    @POST("forecast.json")
    Call<WeatherResponseModel> getWeatherInfo(@Query("key") String api_key,
                                              @Query("q") String locationQuery,
                                              @Query("days") int days);


}
