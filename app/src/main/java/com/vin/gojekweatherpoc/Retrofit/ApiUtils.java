package com.vin.gojekweatherpoc.Retrofit;

import android.content.Context;

import com.vin.gojekweatherpoc.BuildConfig;

/**
 * Created by vinod on 7/7/19.
 */

public class ApiUtils {


    public static AuthApi getAPIInstance(Context context) {
        return BaseServiceGenerator.createService(context, "https://api.apixu.com/v1/").create(AuthApi.class);
    }


}
