package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DayModel implements Serializable {
    @SerializedName("avgtemp_c")
    public String avgtemp_c;
    @SerializedName("condition")
    public LocationConditionInfoModel locationConditionInfoModel;

}

