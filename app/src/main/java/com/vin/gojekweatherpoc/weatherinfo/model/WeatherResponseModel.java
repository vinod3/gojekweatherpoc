package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WeatherResponseModel implements Serializable {

    @SerializedName("location")
    public LocationInfomodel locationInfomodel;
    @SerializedName("current")
    public LocationCurrentInfoModel locationCurrentInfoModel;
    @SerializedName("forecast")
    public LocationForecastInfoModel locationForecastInfoModel;
}
