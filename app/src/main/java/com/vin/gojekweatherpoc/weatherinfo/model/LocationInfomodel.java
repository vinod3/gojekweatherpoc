package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LocationInfomodel implements Serializable {

    @SerializedName("name")
    public String name;
    @SerializedName("region")
    public String region;
    @SerializedName("country")
    public String country;
    @SerializedName("lat")
    public double lat;
    @SerializedName("lon")
    public double lon;
    @SerializedName("tz_id")
    public String tz_id;
    @SerializedName("localtime_epoch")
    public String localtime_epoch;
    @SerializedName("localtime")
    public String localtime;



}
