package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LocationConditionInfoModel implements Serializable {

    @SerializedName("text")
    public  String text;

}
