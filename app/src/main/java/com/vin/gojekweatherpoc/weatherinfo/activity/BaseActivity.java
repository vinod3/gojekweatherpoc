package com.vin.gojekweatherpoc.weatherinfo.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vin.gojekweatherpoc.R;

public abstract class BaseActivity extends AppCompatActivity {

    public static Context sContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        sContext = this;
    }


    public abstract void onRetryNetwork();

    public abstract void initializeViews();

    /**
     * @return boolean true if it is connecting to internet else false
     */
    public static boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) sContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Show no internet popup
     */
    public void showNoInternerPopup() {
        final Dialog mNoInternetDialog = new Dialog(BaseActivity.this);
        mNoInternetDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mNoInternetDialog.setContentView(R.layout.dailog_no_network);
        mNoInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mNoInternetDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        mNoInternetDialog.setCancelable(true);

        TextView retryBtn = mNoInternetDialog.findViewById(R.id.tv_retry);
        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnectingToInternet()) {
                    onRetryNetwork();
                    mNoInternetDialog.dismiss();
                } else {
                    showToast(getResources().getString(R.string.still_no_internet_text));
                }

            }
        });

        mNoInternetDialog.show();

    }


    /**
     * method to show toast
     *
     * @param msg
     */
    public void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
