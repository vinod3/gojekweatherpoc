package com.vin.gojekweatherpoc.weatherinfo.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.vin.gojekweatherpoc.R;
import com.vin.gojekweatherpoc.helper.AppConstants;
import com.vin.gojekweatherpoc.helper.GPSTracker;

public class SplashActivity extends BaseActivity {


    GPSTracker gpsTracker;
    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initializeViews();
    }

    @Override
    public void onRetryNetwork() {

        if (isConnectingToInternet()) {
            proceed();
        } else {
            showToast(getResources().getString(R.string.still_no_internet_text));
        }


    }

    @Override
    public void initializeViews() {

        gpsTracker = new GPSTracker(this);


        if (isConnectingToInternet()) {
            proceed();
        } else {
            showNoInternerPopup();
        }

    }

    private void proceed() {
        if (isLocationPermissionEnabled()) {
            if (gpsTracker.canGetLocation()) {
                navigateToWeatherInfoScreen();
            } else {
                displayLocationSettingsRequest(this);
            }

        } else {
            requestAccessLocationPermission();
        }
    }

    private void navigateToWeatherInfoScreen() {
        Intent i = new Intent(SplashActivity.this, WeatherInfoActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * check permission to access location
     *
     * @return
     */
    private boolean isLocationPermissionEnabled() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request location permission
     */
    public void requestAccessLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        int requestCode = AppConstants.REQUEST_ACCESS_LOCATION_PERMISSION;
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case AppConstants.REQUEST_ACCESS_LOCATION_PERMISSION:
                if (isLocationPermissionEnabled()) {
                    proceed();
                } else {
                    showInfoAbtPermissionAccess();
                }
                break;

        }
    }

    private void showInfoAbtPermissionAccess() {
        final Dialog permissionInfoDialog = new Dialog(this, android.R.style.Theme_Light);
        permissionInfoDialog.setCancelable(true);
        permissionInfoDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        permissionInfoDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        permissionInfoDialog.setContentView(R.layout.dialog_permission_info);
        permissionInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView mOkBtn = permissionInfoDialog.findViewById(R.id.tv_okay);
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestAccessLocationPermission();
                permissionInfoDialog.dismiss();
            }
        });

        permissionInfoDialog.show();
    }

    private void displayLocationSettingsRequest(Context context) {
        try {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000 / 2);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            initializeViews();
                            Log.i("TAG", "All location settings are satisfied.");
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.i("TAG", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result
                                status.startResolutionForResult(SplashActivity.this, REQUEST_CHECK_LOCATION_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                Log.i("TAG", "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.i("TAG", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CHECK_LOCATION_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("TAG", "User agreed to make required location settings changes.");
                        navigateToWeatherInfoScreen();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("TAG", "User chose not to make required location settings changes.");
                        showLocationAccessInfo();
                        break;
                }
                break;

        }

    }

    private void showLocationAccessInfo() {
        final Dialog permissionInfoDialog = new Dialog(this, android.R.style.Theme_Light);
        permissionInfoDialog.setCancelable(true);
        permissionInfoDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        permissionInfoDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        permissionInfoDialog.setContentView(R.layout.dialog_location_info);
        permissionInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView OkBtn = permissionInfoDialog.findViewById(R.id.tv_okay);
        TextView exitBtn = permissionInfoDialog.findViewById(R.id.tv_exit);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permissionInfoDialog.dismiss();
                finish();
            }
        });

        OkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayLocationSettingsRequest(getApplicationContext());
                permissionInfoDialog.dismiss();
            }
        });

        permissionInfoDialog.show();
    }
}
