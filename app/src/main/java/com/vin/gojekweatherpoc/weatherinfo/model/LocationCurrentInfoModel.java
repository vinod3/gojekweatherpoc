package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LocationCurrentInfoModel implements Serializable {

    @SerializedName("last_updated_epoch")
    public String last_updated_epoch;
    @SerializedName("last_updated")
    public String last_updated;
    @SerializedName("temp_c")
    public String temp_c;
    @SerializedName("temp_f")
    public String temp_f;
    @SerializedName("wind_kph")
    public String wind_kph;
    @SerializedName("humidity")
    public String humidity;
    @SerializedName("condition")
    public LocationConditionInfoModel locationConditionInfoModel;



}
