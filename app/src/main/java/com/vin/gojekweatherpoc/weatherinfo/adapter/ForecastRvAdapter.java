package com.vin.gojekweatherpoc.weatherinfo.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.vin.gojekweatherpoc.R;
import com.vin.gojekweatherpoc.databinding.ItemForecastBinding;
import com.vin.gojekweatherpoc.helper.MyTimeUtil;
import com.vin.gojekweatherpoc.weatherinfo.model.ForecastDayModel;

import java.util.List;

public class ForecastRvAdapter extends RecyclerView.Adapter<ForecastRvAdapter.ViewHolder> {

    private ItemForecastBinding mBinding;
    private List<ForecastDayModel> mForecastList;
    private Context mContext;

    public ForecastRvAdapter(Context context, List<ForecastDayModel> forecastDayList) {
        this.mContext = context;
        this.mForecastList = forecastDayList;
    }


    @NonNull
    @Override
    public ForecastRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.item_forecast, viewGroup, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastRvAdapter.ViewHolder viewHolder, int i) {
        mBinding.tvDay.setText(MyTimeUtil.getDayOfWeek(mForecastList.get(i).date_epoch));
        mBinding.tvAvgTemp.setText(mForecastList.get(i).day.avgtemp_c + " " + (char) 0x00B0 + "C");
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemForecastBinding itemForecastBinding;

        public ViewHolder(@NonNull ItemForecastBinding binding) {
            super(binding.getRoot());
            itemForecastBinding = binding;

        }
    }
}
