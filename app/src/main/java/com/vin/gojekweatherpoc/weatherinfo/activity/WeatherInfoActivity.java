package com.vin.gojekweatherpoc.weatherinfo.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.vin.gojekweatherpoc.R;
import com.vin.gojekweatherpoc.Retrofit.ApiUtils;
import com.vin.gojekweatherpoc.Retrofit.AuthApi;
import com.vin.gojekweatherpoc.helper.AppConstants;
import com.vin.gojekweatherpoc.helper.GPSTracker;
import com.vin.gojekweatherpoc.weatherinfo.adapter.ForecastRvAdapter;
import com.vin.gojekweatherpoc.weatherinfo.model.WeatherResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherInfoActivity extends BaseActivity {

    private Animation mRotateAnim;

    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 2;

    AppCompatImageView ivProgress;
    LinearLayout llCurrentWeather, llBottomSheet;
    TextView tvCity, tvTemp;
    LinearLayoutManager layoutManager;
    RecyclerView rvForecast;
    ForecastRvAdapter adapter;

    BottomSheetBehavior bottomSheetBehavior;
    RelativeLayout rlErrorWeather;
    Button btnRetry;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_info);

        initializeViews();
    }

    @Override
    public void onRetryNetwork() {


    }

    @Override
    public void initializeViews() {

        ivProgress = findViewById(R.id.iv_progress);
        llCurrentWeather = findViewById(R.id.ll_current_weather);
        tvCity = findViewById(R.id.tv_city);
        tvTemp = findViewById(R.id.tv_current_temp);
        layoutManager = new LinearLayoutManager(this);
        rvForecast = findViewById(R.id.rv_forecast);
        llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        rlErrorWeather = findViewById(R.id.rl_weather_error);
        rlErrorWeather.setVisibility(View.GONE);
        btnRetry = findViewById(R.id.btn_error_retry);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initializeViews();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        }, 1500);


        // load the animation
        mRotateAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        ivProgress.setVisibility(View.VISIBLE);
        ivProgress.startAnimation(mRotateAnim);
        checkGPSStatus();

    }

    private void checkGPSStatus() {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            getUserCurrentLatLong();
        } else {
            displayLocationSettingsRequest(this);
        }
    }

    private void getUserCurrentLatLong() {
        try {
            GPSTracker gpsTracker = new GPSTracker(sContext);
            if (gpsTracker.canGetLocation()) {
                if (isConnectingToInternet()) {
                    if (gpsTracker.getLongitude() == 0.0 || gpsTracker.getLongitude() == 0.0) {
                        rlErrorWeather.setVisibility(View.VISIBLE);
                    } else {
                        rlErrorWeather.setVisibility(View.GONE);
                        callGetWeatherInfoAPI(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    }

                } else {
                    showToast("Please check your network connection");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callGetWeatherInfoAPI(double latitude, double longitude) {
        String locQuery = latitude + "," + longitude;
        AuthApi authApi = ApiUtils.getAPIInstance(this);
        authApi.getWeatherInfo(AppConstants.APIXU_API_KEY, locQuery, 4).enqueue(new Callback<WeatherResponseModel>() {
            @Override
            public void onResponse(Call<WeatherResponseModel> call, Response<WeatherResponseModel> response) {
                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);
                llCurrentWeather.setVisibility(View.VISIBLE);
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().locationCurrentInfoModel != null) {
                            tvTemp.setText(response.body().locationCurrentInfoModel.temp_c + " " + (char) 0x00B0 + "C");
                        }

                        if (response.body().locationInfomodel != null) {
                            tvCity.setText(response.body().locationInfomodel.name);
                        }

                        //Setup forecast recyclerview
                        rvForecast.setLayoutManager(layoutManager);
                        adapter = new ForecastRvAdapter(WeatherInfoActivity.this, response.body().locationForecastInfoModel.forecastDayList);
                        rvForecast.setAdapter(adapter);

                    }

                } catch (Exception e) {
                    rlErrorWeather.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                    Log.e("Exception_tag", e.toString());
                }
            }

            @Override
            public void onFailure(Call<WeatherResponseModel> call, Throwable t) {
                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);
                rlErrorWeather.setVisibility(View.VISIBLE);
                Log.e("Error_tag", t.toString());
            }
        });
    }

    private void displayLocationSettingsRequest(Context context) {
        try {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000 / 2);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            initializeViews();
                            Log.i("TAG", "All location settings are satisfied.");
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.i("TAG", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result
                                status.startResolutionForResult(WeatherInfoActivity.this, REQUEST_CHECK_LOCATION_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                Log.i("TAG", "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.i("TAG", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CHECK_LOCATION_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("TAG", "User agreed to make required location settings changes.");
                        getUserCurrentLatLong();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("TAG", "User chose not to make required location settings changes.");
                        showToast("Please check your location settings.");
                        break;
                }
                break;

        }

    }
}
