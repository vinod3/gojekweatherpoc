package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ForecastDayModel implements Serializable {

    @SerializedName("date")
    public String date;
    @SerializedName("date_epoch")
    public long date_epoch;
    @SerializedName("day")
    public DayModel day;
    @SerializedName("astro")
    public AstroModel astro;



}
