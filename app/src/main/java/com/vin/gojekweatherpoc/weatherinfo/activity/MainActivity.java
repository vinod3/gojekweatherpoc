package com.vin.gojekweatherpoc.weatherinfo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vin.gojekweatherpoc.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
