package com.vin.gojekweatherpoc.weatherinfo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LocationForecastInfoModel implements Serializable {

    @SerializedName("forecastday")
    public List<ForecastDayModel> forecastDayList;
}
