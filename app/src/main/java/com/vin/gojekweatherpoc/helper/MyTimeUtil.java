package com.vin.gojekweatherpoc.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTimeUtil {


    public static String getDayOfWeek(long epochTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date dateFormat = new java.util.Date(epochTime * 1000);
        String weekday = sdf.format(dateFormat);
        return weekday;
    }
}
